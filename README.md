# fractal

# How to run

1. Install [Dep](https://github.com/golang/dep)

2. Run `dep ensure`

3. Run `go run main.go`

# Testing

`make test`
