test:
	TREE_API_ENDPOINT='https://kf6xwyykee.execute-api.us-east-1.amazonaws.com' go test ./...

build:
	go build main.go

run:
	go run main.go
