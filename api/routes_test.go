package api

import (
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestDrawRoutes(t *testing.T) {
	assert := assert.New(t)
	gin.SetMode(gin.TestMode)

	router := gin.Default()
	DrawRoutes(router)

	assert.Equal(router.BasePath(), "/", "Had set the correct group")
	// TODO Try to check all routes
}
