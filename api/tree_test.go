package api

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestFindIndicators(t *testing.T) {
	gin.SetMode(gin.TestMode)

	router := gin.Default()
	DrawRoutes(router)

	req, err := http.NewRequest(http.MethodGet, "/tree/input?indicator_ids[]=31&indicator_ids[]=32&indicator_ids[]=1", nil)
	assert.Nil(t, err, "Couldn't create request: %v", err)

	w := httptest.NewRecorder()

	// Perform the request
	router.ServeHTTP(w, req)

	// Check to see if the response was what you expected
	assert.Equal(t, w.Code, http.StatusOK, "Expected to get status %d but instead got %d", http.StatusOK, w.Code)
}
