package api

import "github.com/gin-gonic/gin"

func DrawRoutes(router *gin.Engine) {
	tree := new(Tree)

	v1 := router.Group("/")
	{
		v1.GET("tree/input", tree.FindIndicators)
	}
}
