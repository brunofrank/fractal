package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/brunofrank/fractal-tree/services/tree"
)

type Tree struct {
}

type findIndicatorsParams struct {
	Indicators []int `form:"indicator_ids[]"`
}

func (t *Tree) FindIndicators(c *gin.Context) {
	var params findIndicatorsParams
	c.ShouldBind(&params)

	branches, error := tree.FindByIndicators(params.Indicators)

	if error != nil {
		c.JSON(500, "Erro getting the data")
	} else {
		c.JSON(200, branches)
	}

}
