package main

import (
	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv/autoload"
	"gitlab.com/brunofrank/fractal-tree/api"
)

func main() {
	startApiHandler()
}

func startApiHandler() {
	router := gin.Default()

	api.DrawRoutes(router)

	router.Run()
}
