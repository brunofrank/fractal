package tree

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindByIndicators(t *testing.T) {
	expectedIndicators := []int{5, 14}
	branches, err := FindByIndicators(expectedIndicators)

	assert.Nil(t, err, "Error: %v", err)
	assert.NotEmpty(t, branches)

	for _, branch := range branches {
		for _, subTheme := range branch.SubThemes {
			for _, category := range subTheme.Categories {
				for _, indicator := range category.Indicators {
					assert.Contains(t, expectedIndicators, indicator.ID, "Indicators doesn't present")
				}
			}
		}
	}
}
