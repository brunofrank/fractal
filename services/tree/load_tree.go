package tree

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/parnurzeal/gorequest"
)

type Tree struct {
	branches []Branch
}

type Indicator struct {
	ID   int
	Name string
}

type Category struct {
	ID         int
	Name       string
	Unit       string
	Indicators []Indicator
}

type SubTheme struct {
	ID         int
	Name       string
	Categories []Category
}

type Branch struct {
	ID        int
	Name      string
	SubThemes []SubTheme `json:"sub_themes"`
}

func (t *Tree) Load() error {
	url, err := url.Parse(os.Getenv("TREE_API_ENDPOINT"))
	log.Println("URL:", url)
	if err != nil {
		panic("The Tree api address is not properly configured")
	}

	url.Path = "/production/tree/input"

	r, rb, errs := gorequest.New().
		Get(url.String()).
		Retry(5, 5*time.Second, http.StatusBadRequest, http.StatusInternalServerError).
		End()

	if errs != nil {
		return errs[0]
	}

	if r.StatusCode != 200 {
		return errors.New(rb)
	}

	err = json.Unmarshal([]byte(rb), &t.branches)
	if err != nil {
		return err
	}

	return nil
}
