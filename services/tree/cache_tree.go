package tree

type CacheTree struct {
	index map[int][3]int
}

func (cache *CacheTree) CreateIndexes(branches []Branch) {
	cache.index = make(map[int][3]int)

	for bi, branch := range branches {
		for si, subTheme := range branch.SubThemes {
			for ci, category := range subTheme.Categories {
				for ii := range category.Indicators {
					cache.index[ii] = [3]int{bi, si, ci}
				}
			}
		}
	}
}

func (cache *CacheTree) FindIndicators(indicators []int) ([]Branch, error) {
	// Not implemented Yet
	return nil, nil
}
