package tree

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoad(t *testing.T) {
	tree := Tree{}
	err := tree.Load()
	assert.Nil(t, err, "Couldn't load the tree: %v", err)

	assert.NotEmpty(t, tree.branches)
}
