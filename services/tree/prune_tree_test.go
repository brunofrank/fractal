package tree

import (
	"testing"

	_ "github.com/joho/godotenv/autoload"
	"github.com/stretchr/testify/assert"
)

func TestPruneTree(t *testing.T) {
	branches := []Branch{}

	for _, num := range [3]int{1, 2, 3} {
		indicators := []Indicator{
			Indicator{
				ID: num * 5,
			},
			Indicator{
				ID: num * 6,
			},
			Indicator{
				ID: num * 7,
			},
		}

		category := Category{
			ID:         num,
			Indicators: indicators,
		}

		subTheme := SubTheme{
			ID:         num,
			Categories: []Category{category},
		}

		branch := Branch{
			ID:        num,
			SubThemes: []SubTheme{subTheme},
		}

		branches = append(branches, branch)
	}

	expectedIndicators := []int{5, 14}
	prunedBranches, err := PruneTree(branches, expectedIndicators)

	assert.Nil(t, err, "Error: %v", err)
	assert.NotEmpty(t, prunedBranches)

	for _, branch := range prunedBranches {
		for _, subTheme := range branch.SubThemes {
			for _, category := range subTheme.Categories {
				for _, indicator := range category.Indicators {
					assert.Contains(t, expectedIndicators, indicator.ID, "Indicators doesn't present")
				}
			}
		}
	}
}
