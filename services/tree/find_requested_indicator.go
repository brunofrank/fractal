package tree

func FindByIndicators(indicators []int) ([]Branch, error) {
	tree := Tree{}
	if tree.branches == nil {
		err := tree.Load()

		if err != nil {
			return nil, err
		}
	}

	return PruneTree(tree.branches, indicators)
}
