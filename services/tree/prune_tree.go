package tree

func PruneTree(branches []Branch, reqIndicators []int) ([]Branch, error) {
	newBranches := []Branch{}

	for _, branch := range branches {
		for _, subTheme := range branch.SubThemes {
			for _, category := range subTheme.Categories {

				selectedIndicators := []Indicator{}

				for _, indicator := range category.Indicators {
					if wasIndicatorRequested(reqIndicators, indicator.ID) {
						selectedIndicators = append(selectedIndicators, indicator)
					}
				}

				if len(selectedIndicators) > 0 {
					newCategory := Category{
						ID:         category.ID,
						Name:       category.Name,
						Unit:       category.Unit,
						Indicators: selectedIndicators,
					}

					newSubTheme := SubTheme{
						ID:         subTheme.ID,
						Name:       subTheme.Name,
						Categories: []Category{newCategory},
					}

					newBranch := Branch{
						ID:        branch.ID,
						Name:      branch.Name,
						SubThemes: []SubTheme{newSubTheme},
					}

					newBranches = append(newBranches, newBranch)
				}
			}
		}
	}

	return newBranches, nil
}

func wasIndicatorRequested(indicators []int, indicatorID int) bool {
	for _, currentIndicator := range indicators {
		if indicatorID == currentIndicator {
			return true
		}
	}
	return false
}
